TYPE FaceBuilder_FaceModel
	Head AS FaceBuilder_Head
	Eyes AS FaceBuilder_Eyes
	Hair AS FaceBuilder_Hair
	Mark AS FaceBuilder_Mark
	POS AS INTEGER[]
	IS_Visible AS INTEGER
ENDTYPE

TYPE FaceBuilder_Components
	Head AS FaceBuilder_Head[]
	_HeadCount AS INTEGER
	_MaxHeadCount AS INTEGER
	
	Eyes AS FaceBuilder_Eyes[]
	_EyesCount AS INTEGER
	_MaxEyesCount AS INTEGER
	
	Hair AS FaceBuilder_Hair[]
	_HairCount AS INTEGER
	_MaxHairCount AS INTEGER
	
	Mark AS INTEGER[]
	_MarkCount AS INTEGER
	_MaxMarkCount AS INTEGER
ENDTYPE

GLOBAL FaceBuilder_Library AS FaceBuilder_Components

TYPE FaceBuilder_Head
	Image AS INTEGER
	Sprite AS INTEGER
	Size AS INTEGER[]
	Offset AS INTEGER[]
	ID AS INTEGER
ENDTYPE

TYPE FaceBuilder_Eyes
	Image AS INTEGER
	Sprite AS INTEGER
	Size AS INTEGER[]
	Offset AS INTEGER[]
	ID AS INTEGER
ENDTYPE

TYPE FaceBuilder_Hair
	Image AS INTEGER
	Sprite AS INTEGER
	Size AS INTEGER[]
	Offset AS INTEGER[]
	ID AS INTEGER
ENDTYPE

TYPE FaceBuilder_Mark
	Image AS INTEGER
	Size AS INTEGER[]
ENDTYPE

FUNCTION FaceBuilder_Init()
	
	_FaceBuilder_LoadHeads()
	_FaceBuilder_LoadEyes()
	_FaceBuilder_LoadHairs()
	
ENDFUNCTION

FUNCTION _FaceBuilder_LoadHeads()
	
	_FaceBuilder_LoadHead( "heads/brown_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/brown_2.png", 0, 0, 0, 0 ) 
	_FaceBuilder_LoadHead( "heads/brown_3.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/brown_4.png", 0, 0, 0, 0 )
	
	_FaceBuilder_LoadHead( "heads/tan_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/tan_2.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/tan_3.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/tan_4.png", 0, 0, 0, 0 )
	
	_FaceBuilder_LoadHead( "heads/light_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/light_2.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/light_3.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHead( "heads/light_4.png", 0, 0, 0, 0 )
	
ENDFUNCTION

FUNCTION _FaceBuilder_LoadEyes()
	
	_FaceBuilder_LoadEye( "eyes/blacknar_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/bluedeep_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/bluedeep_2.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/bluesmall_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/bluestan_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/bluetan_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/brownstan_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/graystan_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/pixelblue_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/greendark_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/thicklash.png", 0, 0, 0, 0 )
	
	_FaceBuilder_LoadEye( "eyes/firestan_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/ladyish_1.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/purplestan.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadEye( "eyes/redconf.png", 0, 0, 0, 0 )
	
ENDFUNCTION

FUNCTION _FaceBuilder_LoadHairs()
	
	_FaceBuilder_LoadHair( "hair/blacksam.png", 52, 0, 0, -18 )
	_FaceBuilder_LoadHair( "hair/bluesam.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHair( "hair/bubblegum.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHair( "hair/pippi.png", 0, 0, 0, 0 )
	_FaceBuilder_LoadHair( "hair/pirate.png", 0, 0, 0, 0 )
	
ENDFUNCTION

FUNCTION FaceBuilder_Hide( face AS FaceBuilder_FaceModel )

	face.IS_Visible = 0
	SetSpriteVisible( face.Head.Image, 0 )
	SetSpriteVisible( face.Eyes.Image, 0 )
	SetSpriteVisible( face.Hair.Image, 0 )
	SetSpriteVisible( face.Mark.Image, 0 )
	
ENDFUNCTION 

FUNCTION FaceBuilder_Show( face AS FaceBuilder_FaceModel )
	
	face.IS_Visible = 1
	SetSpriteVisible( face.Head.Image, 1 )
	SetSpriteVisible( face.Eyes.Image, 1 )
	SetSpriteVisible( face.Hair.Image, 1 )
	SetSpriteVisible( face.Mark.Image, 1 )
	
ENDFUNCTION

FUNCTION _FaceBuilder_LoadEye( filename$ AS STRING, width AS INTEGER, height AS INTEGER, offsetX AS INTEGER, offsetY AS INTEGER)
	
	// Declare Container Components
	eyes AS FaceBuilder_Eyes
	dimensions AS INTEGER [2]
	
	// Load the requested image
	eyes.Image = LoadImage( filename$ )
	
	// Define dimensions (w/h)
	
	// Use default width and height if 0 passed in.
	IF ( width = 0 ) THEN width = GetImageWidth( eyes.Image )
	IF ( height = 0 ) THEN height = GetImageHeight( eyes.Image )
		
	dimensions[0] = width
	dimensions[1] = height
	eyes.Size = dimensions
	
	// Set ID
	eyes.ID = FaceBuilder_Library._EyesCount
	
	// Set Image Offset
	offset = INTEGER[2]
	offset[0] = offsetX
	offset[1] = offsetY
	eyes.Offset[1] = offsetY
	
	// Make sure we have enough room in our container array
	IF ( FaceBuilder_Library._EyesCount + 1 >= FaceBuilder_Library._MaxEyesCount )
		
		FaceBuilder_Library.Eyes.length = FaceBuilder_Library.Eyes.length + 100
		FaceBuilder_Library._MaxEyesCount = FaceBuilder_Library.Eyes.length
		
	ENDIF
	
	FaceBuilder_Library.Eyes[FaceBuilder_Library._EyesCount] = eyes
	inc FaceBuilder_Library._EyesCount, 1
	
ENDFUNCTION

FUNCTION _FaceBuilder_LoadHair( filename$ AS STRING, width AS INTEGER, height AS INTEGER, offsetX AS INTEGER, offsetY AS INTEGER)
	
	// Declare Container Components
	hair AS FaceBuilder_Hair
	dimensions AS INTEGER [2]
	
	// Load the requested image
	hair.Image = LoadImage( filename$ )

	
	// Define dimensions (w/h)
	
	// Use default width and height if 0 passed in.
	IF ( width = 0 ) THEN width = GetImageWidth( hair.Image )
	IF ( height = 0 ) THEN height = GetImageWidth( hair.Image )
		
	dimensions[0] = width
	dimensions[1] = height
	hair.Size = dimensions
	
	// Set ID
	hair.ID = FaceBuilder_Library._HairCount
	
	// Set Image Offset
	hair.Offset[0] = offsetX
	hair.Offset[1] = offsetY
	
	// Make sure we have enough room in our container array
	IF ( FaceBuilder_Library._HairCount + 1 >= FaceBuilder_Library._MaxHairCount )
		
		FaceBuilder_Library.Hair.length = FaceBuilder_Library.Hair.length + 100
		FaceBuilder_Library._MaxHairCount = FaceBuilder_Library.Hair.length
		
	ENDIF
	
	FaceBuilder_Library.Hair[FaceBuilder_Library._Haircount] = hair
	inc FaceBuilder_Library._HairCount, 1
	
ENDFUNCTION

FUNCTION _FaceBuilder_LoadHead( filename$ AS STRING, width AS INTEGER, height AS INTEGER, offsetX AS INTEGER, offsetY AS INTEGER ) 
	
	// Declare Container Components
	head AS FaceBuilder_Head
	dimensions AS INTEGER [1]
	
	// Load the requested image
	head.Image =  LoadImage( filename$ )

	
	// Define dimensions (w/h)
	
	// Use default width and height if 0 passed in.
	IF ( width = 0 ) THEN width = GetImageWidth( head.Image )
	IF ( height = 0 ) THEN height = GetImageHeight( head.Image )
		
	dimensions[0] = width
	dimensions[1] = height
	head.Size = dimensions
	
	// Set ID
	head.ID = FaceBuilder_Library._HeadCount
	
	// Set Image Offset
	offset AS INTEGER[2]
	
	offset[0] = offsetX
	offset[1] = offsetY
	head.Offset = offset
	
	// Make sure we have enough room in our container array
	IF ( FaceBuilder_Library._HeadCount + 1 >= FaceBuilder_Library._MaxHeadCount )
		
		FaceBuilder_Library.Head.length = FaceBuilder_Library.Head.length + 100
		FaceBuilder_Library._MaxHeadCount = FaceBuilder_Library.Head.length
		
	ENDIF
	
	FaceBuilder_Library.Head[FaceBuilder_Library._HeadCount] = head
	inc FaceBuilder_Library._HeadCount, 1

ENDFUNCTION
