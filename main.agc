#include "Fighters.agc"
#include "FaceBuilder.agc"
// Project: 1dFight 
// Created: 2014-11-29

// set window properties
SetWindowTitle( "1dFight" )
SetWindowSize( 1024, 768, 0 )

// set display properties
SetVirtualResolution( 1024, 768 )
SetOrientationAllowed( 1, 1, 1, 1 )

FaceBuilder_Init()
Fighters_Init( 2 )

do
	
	frontlinePosHuman AS INTEGER [2]
	frontlinePosMonster AS INTEGER [2]
	
	frontlinePosHuman[0] = 400
	frontlinePosHuman[1] = 350
	
	frontlinePosMonster[0] = 500
	frontlinePosMonster[1] = 350
	
	FOR i =  Fighters_FightState.HumansRemaining TO 0 STEP -1

		offsetX = i * -1 *  ( Fighters_FightState.Humans[i].IMG.Head.Size[0] + 10 )
		
		// Determine Hair Offset
		hairoffset AS INTEGER[2]
		
		hairoffset[0] = 0
		hairoffset[1] = 0
		
		SELECT Fighters_FightState.Humans[i].IMG.Hair.ID
			
			CASE 0
				
				hairoffset[0] = 0 // x
				hairoffset[1] = -18 // y
			ENDCASE
				
			CASE 1
				
				hairoffset[0] = 0
				hairoffset[1] = 0
				
			ENDCASE
				
			CASE 2
				
				hairoffset[0] = 0
				hairoffset[1] = 0
				
			ENDCASE
				
			CASE 3
				
				hairoffset[0] = 0
				hairoffset[1] = 0
			
			ENDCASE
				
		ENDSELECT
		
				
		
		SetSpriteVisible( Fighters_FightState.Humans[i].IMG.Head.Sprite, 1 )
		//SetSpriteVisible( Fighters_FightState.Humans[i].IMG.Eyes.Sprite, 1 )
		SetSpriteVisible( Fighters_FightState.Humans[i].IMG.Hair.Sprite, 1 )
		SetSpritePosition( Fighters_FightState.Humans[i].IMG.Head.Sprite, frontlinePosHuman[0] + offsetX, frontlinePosHuman[1] )
		SetSpritePosition( Fighters_FightState.Humans[i].IMG.Eyes.Sprite, frontlinePosHuman[0] + offsetX, frontlinePosHuman[1] )
		SetSpritePosition( Fighters_FightState.Humans[i].IMG.Hair.Sprite, ( frontlinePosHuman[0] + offsetX ) + hairoffset[0],  ( frontlinePosHuman[1] ) + hairoffset[1] )

	NEXT i
	
	FOR i = Fighters_FightState.MonstersRemaining TO 0 STEP - 1
	
		offsetX = i * ( Fighters_FightState.Monsters[i].IMG.Head.Size[0] + 10 )
		
		SetSpriteVisible( Fighters_FightState.Monsters[i].IMG.Head.Sprite, 1 )
		//SetSpriteVisible( Fighters_FightState.Monsters[i].IMG.Eyes.Sprite, 1 )
		SetSpriteVisible( Fighters_FightState.Monsters[i].IMG.Hair.Sprite, 1 )
		SetSpritePosition( Fighters_FightState.Monsters[i].IMG.Head.Sprite, frontlinePosMonster[0] + offsetX, frontlinePosMonster[1] )
		SetSpritePosition( Fighters_FightState.Monsters[i].IMG.Eyes.Sprite, frontlinePosMonster[0] + offsetX, frontlinePosMonster[1] )
		SetSpritePosition( Fighters_FightState.Monsters[i].IMG.Hair.Sprite, frontlinePosMonster[0] + offsetX, frontlinePosMonster[1] )
		
	NEXT i
		
	
    

    Print( ScreenFPS() )
    Sync()
loop
