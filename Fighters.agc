TYPE Fighter
	HP AS INTEGER   // Hit Points
	EVA AS INTEGER  // Evasion Rating
	DEF AS INTEGER  // Defense Rating
	ATK AS INTEGER  // Attack Rating
	ACC AS INTEGER  // Accuracy Rating
	IMG AS FaceBuilder_FaceModel  // Image Package
	Heads AS FighterImage // State Heads
ENDTYPE

TYPE FighterStats
	Humans AS Fighter[]
	Monsters AS Fighter[]
	HumansRemaining AS INTEGER
	MonstersRemaining AS INTEGER
ENDTYPE

TYPE FighterImage
	Lusty AS FaceBuilder_Head
	Determined AS FaceBuilder_Head
	Flagging AS FaceBuilder_Head
	Doomed AS FaceBuilder_Head
ENDTYPE

GLOBAL Fighters_FightState AS FighterStats

/**
 * Generate Fighters
 *
 * @param INTEGER number : Define the number of fighters to create.
 * 
 * @return [] Fighter : An array of the requested size, containing fighters.
 */
FUNCTION _Fighters_MakeFighters( number AS INTEGER )
	
	// Create container for fighers
	DIM fighters[number] AS Fighter
	
	// Create Fighters
	FOR i = 0 TO number
		f AS Fighter
		f.HP = 15
		f.EVA = 3
		f.DEF = 2
		f.ATK = 7
		f.ACC = 8
		
		fighters[i] = f
		
	NEXT i
	
ENDFUNCTION fighters

/**
 * Initialize Fighter Game Stats.
 *
 * @param INTEGER number : Define the number of fighters to create on each side.
 */
FUNCTION Fighters_Init( number AS INTEGER )
	
	// Initalize fighters
	Fighters_FightState.Humans = Fighters_MakeHumans( number )
	Fighters_FightState.Monsters = Fighters_MakeMonsters( number )
	
	// Assign Metadata
	Fighters_FightState.HumansRemaining = number
	Fighters_FightState.MonstersRemaining = number
	
ENDFUNCTION

/**
 * Create a pack of Monsters
 * 
 * @param INTEGER number : Define the number of monsters to create.
 */
FUNCTION Fighters_MakeMonsters( number AS INTEGER )
	
	DIM monsters[number] AS Fighter
	
	monsters = _Fighters_MakeFighters( number )
	
	FOR i = 0 TO number
		
		f AS Fighter
		
		// Build Head Compsition
		head_selection = ( random( 1, 3 ) * 4 ) - 1 
		eye_selection = random( 0, 10 )
		hair_selection = random( 0, 4 )
		
		// Build the head object and the heads array
		f.IMG.Head.Image = FaceBuilder_Library.Head[head_selection].Image
		f.IMG.Head.Size = FaceBuilder_Library.Head[head_selection].Size
		f.IMG.Head.Sprite = CreateSprite( f.IMG.Head.Image )
		SetSpriteSize( f.IMG.Head.Sprite, f.IMG.Head.Size[0], f.IMG.Head.Size[1] )
		SetSpriteVisible( f.IMG.Head.Sprite, 0 )

		f.IMG.Eyes.Image = FaceBuilder_Library.Eyes[eye_selection].Image
		f.IMG.Eyes.Size = FaceBuilder_Library.Eyes[eye_selection].Size
		f.IMG.Eyes.Sprite = CreateSprite( f.IMG.Eyes.Image )
		SetSpriteSize( f.IMG.Eyes.Sprite, f.IMG.Eyes.Size[0],f.IMG.Eyes.Size[1] )
		SetSpriteVisible( f.IMG.Eyes.Sprite, 0 ) 
		
		f.IMG.Hair.Image = FaceBuilder_Library.Hair[hair_selection].Image
		f.IMG.Hair.Size = FaceBuilder_Library.Hair[hair_selection].Size
		f.IMG.Hair.Sprite = CreateSprite( f.IMG.Hair.Image )
		SetSpriteSize( f.IMG.Hair.Sprite, f.IMG.Hair.Size[0], f.IMG.Hair.Size[1] )
		SetSpriteVisible( f.IMG.Hair.Sprite, 0 )
		
		f.Heads.Lusty = FaceBuilder_Library.Head[head_selection]
		f.Heads.Determined = FaceBuilder_Library.Head[head_selection - 1]
		f.Heads.Flagging = FaceBuilder_Library.Head[head_selection - 2]
		f.Heads.Doomed = Facebuilder_Library.Head[head_selection - 3 ]
		
		monsters[i].IMG = f.IMG
		monsters[i].Heads = f.Heads
	
	NEXT i
	
ENDFUNCTION monsters

/**
 * Create a pack of Monsters
 * 
 * @param INTEGER number : Define the number of humans to create.
 */
FUNCTION Fighters_MakeHumans( number AS INTEGER )

	DIM humans[number] AS Fighter
	
	humans = _Fighters_MakeFighters( number )
	
	FOR i = 0 TO number
		
		f AS Fighter
		
		// Build Head Compsition
		head_selection = ( random( 1, 3 ) * 4 ) - 1 
		eye_selection = random( 0, 10 )
		hair_selection = random( 1, 1 )
		
		// Build the head object and the heads array
		f.IMG.Head.Image = FaceBuilder_Library.Head[head_selection].Image
		f.IMG.Head.Size = FaceBuilder_Library.Head[head_selection].Size
		f.IMG.Head.Sprite = CreateSprite( f.IMG.Head.Image )
		SetSpriteSize( f.IMG.Head.Sprite, f.IMG.Head.Size[0], f.IMG.Head.Size[1] )
		SetSpriteOffset( f.IMG.Head.Sprite, f.IMG.Head.Offset[0], f.IMG.Head.Offset[1] )
		SetSpriteVisible( f.IMG.Head.Sprite, 0 )

		f.IMG.Eyes.Image = FaceBuilder_Library.Eyes[eye_selection].Image
		f.IMG.Eyes.Size = FaceBuilder_Library.Eyes[eye_selection].Size
		f.IMG.Eyes.Sprite = CreateSprite( f.IMG.Eyes.Image )
		SetSpriteSize( f.IMG.Eyes.Sprite, f.IMG.Eyes.Size[0],f.IMG.Eyes.Size[1] )
		SetSpriteOffset( f.IMG.Head.Sprite, f.IMG.Head.Offset[0], f.IMG.Head.Offset[1] )
		SetSpriteVisible( f.IMG.Eyes.Sprite, 0 ) 
		
		f.IMG.Hair.Image = FaceBuilder_Library.Hair[hair_selection].Image
		f.IMG.Hair.Size = FaceBuilder_Library.Hair[hair_selection].Size
		f.IMG.Hair.Sprite = CreateSprite( f.IMG.Hair.Image )
		SetSpriteSize( f.IMG.Hair.Sprite, f.IMG.Hair.Size[0], f.IMG.Hair.Size[1] )
		SetSpriteOffset( f.IMG.Hair.Sprite, f.IMG.Hair.Offset[0], f.IMG.Hair.Offset[1] )
		SetSpriteVisible( f.IMG.Hair.Sprite, 0 )
		
		f.Heads.Lusty = FaceBuilder_Library.Head[head_selection]
		f.Heads.Determined = FaceBuilder_Library.Head[head_selection - 1]
		f.Heads.Flagging = FaceBuilder_Library.Head[head_selection - 2]
		f.Heads.Doomed = Facebuilder_Library.Head[head_selection - 3 ]
		
		humans[i].IMG = f.IMG
		humans[i].Heads = f.Heads
		
	NEXT i 
	
ENDFUNCTION humans




